<?php

namespace Drupal\mvola\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingForm.
 */
class MvolaSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'mvola.setting',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'setting_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('mvola.setting');
    $form['mvola_url'] = array(
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Mvola URL')
    );
    $form['mvola_url']['sandbox_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sandbox'),
      '#maxlength' => 64,
      '#size' => 64,
      '#required' => TRUE,
      '#description' => $this->t('https://devapi.mvola.mg'),
      '#default_value' => $config->get('sandbox_url'),
    ];
    $form['mvola_url']['production_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Production'),
      '#maxlength' => 64,
      '#size' => 64,
      '#required' => TRUE,
      '#description' => $this->t('https://api.mvola.mg'),
      '#default_value' => $config->get('production_url'),
    ];
    $form['mvola_url']['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mvola Path'),
      '#maxlength' => 64,
      '#size' => 64,
      '#required' => TRUE,
      '#description' => $this->t('/mvola/mm/transactions/type/merchantpay/1.0.0/'),
      '#default_value' => $config->get('path'),
    ];
    $form['consumer'] = array(
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Mvola API Key and Secret')
    );
    $form['consumer']['consumerKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Consumer key'),
      '#maxlength' => 64,
      '#size' => 64,
      '#required' => TRUE,
      '#default_value' => $config->get('consumerKey'),
    ];
    $form['consumer']['consumerSecret'] = [
      '#type' => 'password',
      '#title' => $this->t('Consumer secret'),
      '#maxlength' => 64,
      '#size' => 64,
      '#required' => TRUE,
      '#default_value' => $config->get('consumerSecret'),
      '#description' => $this->t('Get here : <a target="_blank" href="https://www.mvola.mg/devportal">https://www.mvola.mg/devportal</a>'),
    ];
    $form['partner'] = array(
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Mvola Partner')
    );
    $form['partner']['partnerName'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Partner Name'),
      '#maxlength' => 64,
      '#size' => 64,
      '#required' => TRUE,
      '#default_value' => $config->get('partnerName'),
      '#description' => $this->t('Example : TestMvola '),
    ];
    $form['partner']['partnerID'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Partner Phone number'),
      '#maxlength' => 64,
      '#size' => 64,
      '#required' => TRUE,
      '#default_value' => $config->get('partnerID'),
      '#description' => $this->t('Example : 0343500004 '),
    ];
    $form['mode']=[
      '#type' => 'select',
      '#title' => t('Mode'),
      '#required' => TRUE,
      '#options' => ['Sandbox','Production'],
      '#default_value' => $config->get('mode')
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $values = $form_state->getValues();
    $this->config('mvola.setting')
      ->set('sandbox_url', trim($values['sandbox_url']))
      ->set('production_url', trim($values['production_url']))
      ->set('path', trim($values['path']))
      ->set('consumerKey', trim($values['consumerKey']))
      ->set('consumerSecret', trim($values['consumerSecret']))
      ->set('partnerName', trim($values['partnerName']))
      ->set('partnerID', trim($values['partnerID']))
      ->set('mode', trim($values['mode']))
      ->save();
  }

}
