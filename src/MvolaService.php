<?php

namespace Drupal\mvola;

use Drupal\Component\Serialization\Json;

/**
 * Class MvolaService.
 */
class MvolaService
{
    public $url = false;
    public $path = false;
    public $mode = false;
    public $partnerID = false;
    public $partnerName = false;
    public $consumerKey = false;
    private $consumerSecret = false;
    /**
     * Constructs a new MvolaService object.
     */
    public function __construct()
    {
        $mvola_config = \Drupal::config('mvola.setting');
        if ($mvola_config->get('sandbox_url')
            && $mvola_config->get('production_url')
            && $mvola_config->get('path')
            && is_numeric($mvola_config->get('mode'))
            && $mvola_config->get('partnerName')
            && $mvola_config->get('partnerID')
            && $mvola_config->get('consumerKey')
            && $mvola_config->get('consumerSecret')
        ) {

            $this->url = ($mvola_config->get('mode') == 0) ? $mvola_config->get('sandbox_url') : $mvola_config->get('production_url');
            $this->path = $mvola_config->get('path');
            $this->mode = $mvola_config->get('mode');
            $this->partnerID = $mvola_config->get('partnerID');
            $this->partnerName = $mvola_config->get('partnerName');
            $this->consumerKey = $mvola_config->get('consumerKey');
            $this->consumerSecret = $mvola_config->get('consumerSecret');
        } else {
            \Drupal::messenger()->addMessage(t('Please set up the mvola configuration first here: /admin/config/mvola/settings '), 'error');
            \Drupal::logger('mvola')->error('Please set up the mvola configuration first here: /admin/config/mvola/settings');
        }
    }
    public function generateToken()
    {
        try {
            if ($this->url && $this->consumerKey && $this->consumerSecret) {
                $data = ['grant_type' => 'client_credentials', 'scope' => 'EXT_INT_MVOLA_SCOPE'];
                $url = $this->url;
                $auth = base64_encode($this->consumerKey . ":" . $this->consumerSecret);
                $header = [
                    "Content-Type" => "application/x-www-form-urlencoded",
                    "Cache-Control" => "no-cache",
                    "Authorization" => "Basic " . $auth,
                ];
                $response = \Drupal::httpClient()
                    ->post($url . '/token', [
                        'verify' => true,
                        'form_params' => $data,
                        'headers' => $header,
                    ]);
                $result_json = $response->getBody()->getContents();
                $results = Json::decode($result_json);
                if (isset($results["access_token"])) {
                    return $results["access_token"];
                } else {
                    \Drupal::logger('mvola')->error('Token Request is failed :' . $result_json);
                    return false;
                }
            } else {return false;}
        } catch (\Exception $e) {
            \Drupal::logger('mvola')->error('Token Request is failed :' . $e->getMessage());
            return false;
        }

    }
    private function getDataTransactionFormatter($amount, $debitParty, $description)
    {
        $date = (new \DateTime('UTC'))->format('Y-m-d\TH:i:s.v\Z');
        $data = [
            'amount' => $amount,
            'currency' => 'Ar',
            'descriptionText' => $description,
            'requestDate' => $date,
            'requestingOrganisationTransactionReference' => 'transactionRef',
            'originalTransactionReference' => 'transactionRef',
            'debitParty' => [
                0 => [
                    'key' => 'msisdn',
                    'value' => $debitParty,
                ],
            ],
            'creditParty' => [
                0 => [
                    'key' => 'msisdn',
                    'value' => $this->partnerID,
                ],
            ],
            'metadata' => [
                0 => [
                    'key' => 'partnerName',
                    'value' => $this->partnerName,
                ],
                1 => [
                    'key' => 'fc',
                    'value' => 'USD',
                ],
                2 => [
                    'key' => 'amountFc',
                    'value' => '1',
                ],
            ],
        ];
        return $data;
    }
    public function getTransactionStatus($serverCorrelationId)
    {
        try {
            $uuid = \Drupal::service('uuid')->generate();
            $token = $this->generateToken();
            if (is_string($token) && $this->partnerID && $this->partnerID && $this->path) {
                $header = [
                    'Content-Type' => 'application/json',
                    'Cache-Control' => 'no-cache',
                    'Version' => '1.0',
                    'UserLanguage' => 'MG',
                    'Authorization' => 'Bearer ' . $token,
                    'x-correlationid' => $uuid,
                    'UserAccountIdentifier' => 'msisdn;' . $this->partnerID,
                    'partnerName' => $this->partnerName,
                ];
                $path = $this->path . "/status" . "/" . $serverCorrelationId;
                $serialized_entity = json_encode($data);
                $response = \Drupal::httpClient()
                    ->get($this->url . $path, [
                        'verify' => true,
                        'headers' => $header,
                    ]);
                $result_json = $response->getBody()->getContents();
                return Json::decode($result_json);
            } else {
                return false;
            }
        } catch (\Exception $e) {
            \Drupal::logger('mvola')->error('Get TransactionStatus Payement is failed :' . $e->getMessage());
        }
    }
    public function getTransactionDetails($transID)
    {
        try {
            $uuid = \Drupal::service('uuid')->generate();
            $token = $this->generateToken();
            if (is_string($token) && $this->partnerID && $this->partnerID && $this->path) {
                $header = [
                    'Content-Type' => 'application/json',
                    'Cache-Control' => 'no-cache',
                    'Version' => '1.0',
                    'UserLanguage' => 'MG',
                    'Authorization' => 'Bearer ' . $token,
                    'x-correlationid' => $uuid,
                    'UserAccountIdentifier' => 'msisdn;' . $this->partnerID,
                    'partnerName' => $this->partnerName,
                ];
                $path = $this->path  . "/" . $transID;
                $serialized_entity = json_encode($data);
                $response = \Drupal::httpClient()
                    ->get($this->url . $path, [
                        'verify' => true,
                        'headers' => $header,
                    ]);
                $result_json = $response->getBody()->getContents();
                return Json::decode($result_json);
            } else {
                return false;
            }
        } catch (\Exception $e) {
            \Drupal::logger('mvola')->error('Get TransactionStatus Payement is failed :' . $e->getMessage());
        }
    }
    public function sendPayment($amount, $debitParty, $description)
    {
        try {
            $data = $this->getDataTransactionFormatter($amount, $debitParty, $description);
            $uuid = \Drupal::service('uuid')->generate();
            $token = $this->generateToken();
            if (is_string($token) && $this->partnerID && $this->partnerID && $this->path) {
                $header = [
                    'Content-Type' => 'application/json',
                    'Cache-Control' => 'no-cache',
                    'Version' => '1.0',
                    'UserLanguage' => 'MG',
                    'Authorization' => 'Bearer ' . $token,
                    'x-correlationid' => $uuid,
                    'UserAccountIdentifier' => 'msisdn;' . $this->partnerID,
                    'partnerName' => $this->partnerName,
                ];
                $serialized_entity = json_encode($data);
                $response = \Drupal::httpClient()
                    ->post($this->url . $this->path, [
                        'verify' => true,
                        'body' => $serialized_entity,
                        'headers' => $header,
                    ]);
                $result_json = $response->getBody()->getContents();
                $results = Json::decode($result_json);
                if (isset($results["status"])) {
                    \Drupal::logger('mvola')->info('Send Request is succesfully :' . $result_json);
                    return $results;
                } else {
                    \Drupal::logger('mvola')->error('Send Request is failed :' . $result_json);
                    return false;
                }
            } else {
                return false;
            }
        } catch (\Exception $e) {
            \Drupal::logger('mvola')->error('Send Payement Request is failed :' . $e->getMessage());
        }
    }
}
